import asyncio

from hypercorn import Config
from hypercorn.asyncio import serve


def run_api() -> None:
    from src.api.main import app
    config = Config()
    config.bind = ["0.0.0.0:8000"]
    asyncio.run(serve(app, config))


def main() -> None:
    run_api()


if __name__ == '__main__':
    main()
