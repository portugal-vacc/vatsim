from typing import List

from fastapi import APIRouter

from src.api.v1.schemas.vatsimdata import VatsimDataPilot, VatsimDataController, VatsimDataGeneral, VatsimDataAtis, \
    VatsimDataServer, VatsimDataPrefile, VatsimDataFacility, VatsimDataRating, VatsimDataPilotRating
from src.api.v1.services import get_vatsim_data

live_router = APIRouter(prefix='/live', tags=['vatsim:live'])


@live_router.get("/general", response_model=VatsimDataGeneral)
def general():
    return get_vatsim_data().general


@live_router.get("/pilots", response_model=List[VatsimDataPilot])
def pilots():
    return get_vatsim_data().pilots


@live_router.get("/pilots/{country_code}", response_model=List[VatsimDataPilot])
def pilots_from_to_country(country_code: str):
    to_return = []
    for this_pilot in get_vatsim_data().pilots:
        if this_pilot.flight_plan:
            if this_pilot.flight_plan.departure.startswith(
                    country_code.upper()) or this_pilot.flight_plan.arrival.startswith(country_code.upper()):
                to_return.append(this_pilot)
    return to_return


@live_router.get("/controllers", response_model=List[VatsimDataController])
def controllers():
    return get_vatsim_data().controllers


@live_router.get("/controllers/{country_code}", response_model=List[VatsimDataController])
def country_controllers(country_code: str):
    return [this_controller for this_controller in get_vatsim_data().controllers if
            str(this_controller.callsign).startswith(country_code.upper())]


@live_router.get("/atis", response_model=List[VatsimDataAtis])
def atis():
    return get_vatsim_data().atis


@live_router.get("/atis/{country_code}", response_model=List[VatsimDataAtis])
def country_atis(country_code: str):
    return [this_atis for this_atis in get_vatsim_data().atis if
            str(this_atis.callsign).startswith(country_code.upper())]


@live_router.get("/servers", response_model=List[VatsimDataServer])
def servers():
    return get_vatsim_data().servers


@live_router.get("/prefiles", response_model=List[VatsimDataPrefile])
def prefiles():
    return get_vatsim_data().prefiles


@live_router.get("/facilities", response_model=List[VatsimDataFacility])
def facilities():
    return get_vatsim_data().facilities


@live_router.get("/ratings", response_model=List[VatsimDataRating])
def ratings():
    return get_vatsim_data().ratings


@live_router.get("/pilot_ratings", response_model=List[VatsimDataPilotRating])
def pilot_ratings():
    return get_vatsim_data().pilot_ratings
