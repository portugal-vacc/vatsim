from typing import List, Optional

import httpx
from cachetools import TTLCache, cached

from src.api.v1.schemas.clients import Client, ClientConnections, ClientFlightPlans, ClientRatingTimes
from src.api.v1.schemas.events import Event
from src.api.v1.schemas.vatsimdata import VatsimData


def get_request(url: str) -> dict:
    response = httpx.get(url)
    response.raise_for_status()
    return response.json()


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_all_events() -> List[Optional[Event]]:
    data = get_request('https://my.vatsim.net/api/v1/events/all')
    events = data['data']
    return [Event(**event) for event in events]


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_vatsim_data() -> VatsimData:
    data = get_request('https://data.vatsim.net/v3/vatsim-data.json')
    return VatsimData(**data)


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_client(client_id: int) -> Client:
    data = get_request(f'https://api.vatsim.net/api/ratings/{client_id}/')
    return Client(**data)


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_client_connections(client_id: int) -> ClientConnections:
    data = get_request(f'https://api.vatsim.net/api/ratings/{client_id}/connections/')
    return ClientConnections(**data)


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_client_flight_plans(client_id: int) -> ClientFlightPlans:
    data = get_request(f'https://api.vatsim.net/api/ratings/{client_id}/flight_plans/')
    return ClientFlightPlans(**data)


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_client_rating_times(client_id: int) -> ClientRatingTimes:
    data = get_request(f'https://api.vatsim.net/api/ratings/{client_id}/rating_times/')
    return ClientRatingTimes(**data)
