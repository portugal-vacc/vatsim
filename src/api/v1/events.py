from typing import List, Optional

from fastapi import APIRouter

from src.api.v1.schemas.events import Event
from src.api.v1.storage import events_data

events_router = APIRouter(prefix='/events', tags=['vatsim:events'])


def is_portugal_event(event: Event) -> bool:
    portugal_in_routes = [route for route in event.routes if
                          route.departure.startswith('LP') or route.arrival.startswith('LP')]
    portugal_in_airport = [airport for airport in event.airports if airport.icao.startswith("LP")]
    return True if portugal_in_routes or portugal_in_airport else False


def parse_portugal_events(all_events: List[Optional[Event]]) -> List[Optional[Event]]:
    return [event for event in all_events if is_portugal_event(event)]


@events_router.get("/portugal", response_model=List[Optional[Event]])
def portugal():
    return parse_portugal_events(events_data())
