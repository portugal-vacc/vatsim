import os
from datetime import datetime, timedelta
from pathlib import Path
from typing import List, Optional

from pydantic import BaseModel

from src.api.v1.schemas.events import Event
from src.api.v1.services import get_all_events

FILE_CACHE_LOCATION = Path('./cache.json')


class StorageProtocol(BaseModel):
    ttl_minutes: int = 3600
    retrieved_at: datetime = None
    events: List[Optional[Event]]

    @property
    def is_outdated(self):
        return True if datetime.now() - self.retrieved_at > timedelta(minutes=self.ttl_minutes) else False


def read_cache() -> None | str:
    try:
        with open(FILE_CACHE_LOCATION, 'r') as file:
            return file.read()
    except FileNotFoundError:
        return None


def write_cache(data) -> None:
    FILE_CACHE_LOCATION.touch(exist_ok=True)
    with open(FILE_CACHE_LOCATION, 'w+') as file:
        file.write(data)


def get_events(ttl_minutes: int) -> StorageProtocol:
    data: List[Optional[Event]] = get_all_events()
    return StorageProtocol(ttl_minutes=ttl_minutes, retrieved_at=datetime.now(),
                           events=data)


def events_data(force: bool = False) -> List[Optional[Event]]:
    ttl_minutes = os.getenv('TIME_TO_LIVE_MINUTES', 30)
    cache = read_cache()
    if force or not cache:
        protocol = get_events(ttl_minutes=ttl_minutes)
        write_cache(protocol.json())
        return protocol.events
    protocol = StorageProtocol.parse_raw(cache)
    if protocol.is_outdated:
        protocol = get_events(ttl_minutes=ttl_minutes)
        write_cache(protocol.json())
    return protocol.events
