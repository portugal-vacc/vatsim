import re
from typing import List, Optional, Union

from pydantic import BaseModel


class PropertyBaseModel(BaseModel):
    """
    Workaround for serializing properties with pydantic until
    https://github.com/samuelcolvin/pydantic/issues/935
    is solved
    """

    @classmethod
    def get_properties(cls):
        return [prop for prop in dir(cls) if
                isinstance(getattr(cls, prop), property) and prop not in ("__values__", "fields")]

    def dict(
            self,
            *,
            include: Union['AbstractSetIntStr', 'MappingIntStrAny'] = None,
            exclude: Union['AbstractSetIntStr', 'MappingIntStrAny'] = None,
            by_alias: bool = False,
            skip_defaults: bool = None,
            exclude_unset: bool = False,
            exclude_defaults: bool = False,
            exclude_none: bool = False,
    ) -> 'DictStrAny':
        attribs = super().dict(
            include=include,
            exclude=exclude,
            by_alias=by_alias,
            skip_defaults=skip_defaults,
            exclude_unset=exclude_unset,
            exclude_defaults=exclude_defaults,
            exclude_none=exclude_none
        )
        props = self.get_properties()
        # Include and exclude properties
        if include:
            props = [prop for prop in props if prop in include]
        if exclude:
            props = [prop for prop in props if prop not in exclude]

        # Update the attribute dict with the properties
        if props:
            attribs.update({prop: getattr(self, prop) for prop in props})

        return attribs


class VatsimDataGeneral(BaseModel):
    version: int
    reload: int
    update: str
    update_timestamp: str
    connected_clients: int
    unique_users: int


class VatsimDataFlightPlan(BaseModel):
    flight_rules: str
    aircraft: str
    aircraft_faa: Optional[str]
    aircraft_short: str
    departure: str
    arrival: str
    alternate: Optional[str]
    cruise_tas: str
    altitude: str
    deptime: str
    enroute_time: str
    fuel_time: str
    remarks: str
    route: str
    revision_id: int
    assigned_transponder: str


class VatsimDataPilot(BaseModel):
    cid: int
    name: str
    callsign: str
    server: str
    pilot_rating: int
    latitude: float
    longitude: float
    altitude: int
    groundspeed: int
    transponder: str
    heading: int
    qnh_i_hg: float
    qnh_mb: int
    flight_plan: Optional[VatsimDataFlightPlan]
    logon_time: str
    last_updated: str


class VatsimDataController(BaseModel):
    cid: int
    name: str
    callsign: str
    frequency: str
    facility: int
    rating: int
    server: str
    visual_range: int
    text_atis: Optional[List[str]]
    last_updated: str
    logon_time: str


class VatsimDataAtis(PropertyBaseModel):
    cid: int
    name: str
    callsign: str
    frequency: str
    rating: int
    server: str
    visual_range: int
    atis_code: Optional[str]
    text_atis: Optional[List[str]]
    last_updated: str
    logon_time: str

    @property
    def full_atis(self) -> str:
        return ''.join([f" {atis}" for atis in self.text_atis]) if self.text_atis else ''

    @property
    def lvp(self) -> bool:
        return True if "LOW VIS PROCEDURES IN FORCE" in self.full_atis else False

    @property
    def runway_active(self) -> Optional[str]:
        match = re.search(r"RWY IN USE (?P<runway>\d{2})", self.full_atis)
        return match.group("runway") if match else None


class VatsimDataServer(BaseModel):
    ident: str
    hostname_or_ip: str
    location: str
    name: str
    clients_connection_allowed: int
    client_connections_allowed: bool
    is_sweatbox: bool


class VatsimDataPrefile(BaseModel):
    cid: int
    name: str
    callsign: str
    flight_plan: Optional[VatsimDataFlightPlan]
    last_updated: str


class VatsimDataFacility(BaseModel):
    id: int
    short: str
    long: str


class VatsimDataRating(BaseModel):
    id: int
    short: str
    long: str


class VatsimDataPilotRating(BaseModel):
    id: int
    short_name: str
    long_name: str


class VatsimData(BaseModel):
    general: Optional[VatsimDataGeneral]
    pilots: List[Optional[VatsimDataPilot]]
    controllers: List[Optional[VatsimDataController]]
    atis: List[Optional[VatsimDataAtis]]
    servers: List[Optional[VatsimDataServer]]
    prefiles: List[Optional[VatsimDataPrefile]]
    facilities: List[Optional[VatsimDataFacility]]
    ratings: List[Optional[VatsimDataRating]]
    pilot_ratings: List[Optional[VatsimDataPilotRating]]
