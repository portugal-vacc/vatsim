from typing import Optional, Any, List

from pydantic import BaseModel


class Client(BaseModel):
    id: str
    rating: int
    pilotrating: int
    susp_date: Optional[str]
    reg_date: str
    region: str
    division: Optional[str]
    subdivision: Optional[str]
    lastratingchange: Optional[str]


class ClientConnectionsResult(BaseModel):
    id: int
    vatsim_id: str
    type: int
    rating: int
    callsign: str
    start: str
    end: str
    server: str


class ClientConnections(BaseModel):
    count: int
    next: Optional[Any]
    previous: Optional[Any]
    results: List[Optional[ClientConnectionsResult]]


class ClientFlightPlansResult(BaseModel):
    id: int
    connection_id: int
    vatsim_id: str
    flight_type: str
    callsign: str
    aircraft: Optional[str]
    cruisespeed: str
    dep: str
    arr: str
    alt: Optional[str]
    altitude: str
    rmks: str
    route: str
    deptime: str
    hrsenroute: int
    minenroute: int
    hrsfuel: int
    minsfuel: int
    filed: str
    assignedsquawk: str
    modifiedbycid: Optional[str]
    modifiedbycallsign: Optional[str]


class ClientFlightPlans(BaseModel):
    count: int
    next: Optional[Any]
    previous: Optional[Any]
    results: List[Optional[ClientFlightPlansResult]]


class ClientRatingTimes(BaseModel):
    id: int
    atc: float
    pilot: float
    s1: float
    s2: float
    s3: float
    c1: float
    c2: float
    c3: float
    i1: float
    i2: float
    i3: float
    sup: float
    adm: float
