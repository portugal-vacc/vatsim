from typing import Optional, List, Any

from pydantic import BaseModel


class EventsOrganizer(BaseModel):
    region: Optional[str]
    division: Optional[str]
    subdivision: Optional[str]
    organised_by_vatsim: bool


class EventsAirport(BaseModel):
    icao: str


class EventsRoute(BaseModel):
    departure: str
    arrival: str
    route: Any


class Event(BaseModel):
    id: int
    type: str
    vso_name: Optional[str]
    name: str
    link: Optional[str]
    organizers: Optional[List[EventsOrganizer]]
    airports: List[Optional[EventsAirport]]
    routes: List[Optional[EventsRoute]]
    start_time: str
    end_time: str
    short_description: str
    description: str
    banner: str
