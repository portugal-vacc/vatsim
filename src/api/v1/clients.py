from fastapi import APIRouter

from src.api.v1.schemas.clients import Client, ClientConnections, ClientFlightPlans, ClientRatingTimes
from src.api.v1.services import get_client, get_client_connections, get_client_flight_plans, get_client_rating_times

clients_router = APIRouter(prefix='/clients', tags=['vatsim:clients'])


@clients_router.get("/{cid}", response_model=Client)
def client(cid: int):
    return get_client(cid)


@clients_router.get("/{cid}/connections", response_model=ClientConnections)
def client_connections(cid: int):
    return get_client_connections(cid)


@clients_router.get("/{cid}/flight-plans", response_model=ClientFlightPlans)
def client_flight_plans(cid: int):
    return get_client_flight_plans(cid)


@clients_router.get("/{cid}/times", response_model=ClientRatingTimes)
def client_rating_times(cid: int):
    return get_client_rating_times(cid)
