from fastapi import APIRouter

from src.api.v1.clients import clients_router
from src.api.v1.events import events_router
from src.api.v1.live import live_router

v1_api = APIRouter(prefix='/v1')

v1_api.include_router(live_router)
v1_api.include_router(events_router)
v1_api.include_router(clients_router)
