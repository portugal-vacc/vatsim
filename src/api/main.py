from fastapi import FastAPI, APIRouter
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import RedirectResponse

from src.api.v1.main import v1_api

description = """<h1>For Flight Simulation Only!</h1>"""

app = FastAPI(title="VATSIM DATA API",
              docs_url="/api/docs",
              description=description,
              contact={
                  "name":  "Rodrigo Simões - 1438868",
                  "email": "rodrigo.simoes@portugal-vacc.org",
              },
              license_info={
                  "name": "GNU General Public License v3.0",
                  "url":  "https://www.gnu.org/licenses/gpl-3.0.en.html",
              },
              openapi_url="/api/openapi.json")

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def redirect_to_docs():
    return RedirectResponse("/api/docs")


api = APIRouter(prefix='/api')

api.include_router(v1_api)

app.include_router(api)
